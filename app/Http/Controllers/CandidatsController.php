<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Candidats;
use App\Archive;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;
use File;
use Hash;
use Excel;

class CandidatsController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$all_c2 = Candidats::where('cdt_type', 'C')->get();
		$ciel2 = Candidats::where('cdt_type', 'C')->orderBy('cdt_nom', 'asc')->paginate(25);
		$prepa = Candidats::where('cdt_type', 'P')->orderBy('cdt_nom', 'asc')->get();
		$etrangeres = Candidats::where('cdt_type', 'E')->get();
		$all = Candidats::orderBy('cdt_nom', 'asc')->get();
		$archive = Archive::all()->groupBy('cdt_year');
		
		return view('candidats.data', compact('all_c2', 'ciel2', 'prepa', 'etrangeres', 'all', 'archive'));
	}

	public function change_pass(Request $request)
	{
		$this->validate(request(), [
			'mdp_act' => 'required|string|min:6',
			'mdp_new' => 'required|string|min:6|confirmed',
		]);

		$user = Auth::user();

		if(Hash::check($request->mdp_act, $user->password)){

			$user->password = bcrypt($request->mdp_new);
			$user->save();

			return redirect()->back()->with('message', 'Vous avez bien changé votre mot de passe'); 
		}
		else
		{
			return redirect()->back()->withErrors('Votre ancien mot de passe n\'est pas correct' );
		}
	}

	/*
	* Data import from Excel, Libre Office Calc etc.
	*/
	public function importExcel(Request $request)
	{
		$this->validate(request(), [
			'import_file' => 'file|max:5120|nullable',
		]);

		$message = array(
			'success' => 'Vous avez bien importé des données',
			'error' => 'Vous avez bien importé des données',
		);
		$file = $request->file('import_file');

		if($file){
			$data = Excel::load($file, function($reader) {})->get();
			if(!empty($data) && $data->count()){
				foreach ($data as $list) {
					foreach ($list as $key => $value) {
						$civilite = '';
						if($value->civilite == 'Monsieur'){
							$civilite = 'M';
						} else {
							$civilite = 'Mme';
						}

						$insert[] = [
							'cdt_type' => 'C', // C - ciell2, P - prepa, E - etrangeres et papiers
							'cdt_ciel_id' => $value->id,
							'cdt_civilite' => $civilite,
							'cdt_nom' => $value->nom,
							'cdt_prenom' => $value->prenom,
							'cdt_email' => $value->email,
							'cdt_date_de_naissance' => $value->date_de_naissance,
							'cdt_adresse' => $value->adresse,
							'cdt_cp' => $value->cp,
							'cdt_ville' => $value->ville,
							'cdt_tel_mob' => preg_replace("/\D|\./i", "", $value->tel_mob),
							'cdt_tel_fixe' => preg_replace("/\D|\./i", "", $value->tel_fixe),
							'cdt_nationalite' => $value->nationalite,

							'cdt_formation' => $value->formation,
							'cdt_postbac_etablissement' => $value->formation_etablissement,
							'cdt_postbac_intitule' => $value->formation_intitule,
							'cdt_postbac_lieu' => $value->formation_lieu,
							'cdt_diplome_etablissement' => $value->diplome_etablissement,
							'cdt_diplome_intitule' => $value->diplome_intitule,
							'cdt_diplome_type_etablissement' => $value->diplome_type_etablissement,
							'cdt_diplome_type' => $value->diplome_type,
							'cdt_code_ine_bea' => $value->ine,
						];
					}
				}

				if(!empty($insert)){
					try{
						foreach ($insert as $i) {
							Candidats::updateOrCreate($i);
						}
					} catch(\Exception $e) {
						return redirect()->back()->with('error', $e->errorInfo[2]);
					}

					return redirect()->back()->with('message', $message['success']);
				} else {
					return redirect()->back()->with('error', $message['error']);
				}
			} else {
				return redirect()->back()->with('error', $message['error']);
			}
		}
		return redirect()->back();
	}

	public function show(Candidats $cdt)
	{
		return view('candidats.show', compact('cdt'));
	}

	public function decline(Candidats $cdt)
	{
		$cdt->cdt_status = 'refuse';
		$cdt->save();

		return back();
	}

	public function accept(Candidats $cdt)
	{
		$cdt->cdt_status = 'accepte';
		$cdt->save();

		return back();
	}

	public function listecomp(Candidats $cdt)
	{
		$cdt->cdt_status = 'LC';
		$cdt->save();

		return back();
	}

	public function entretien(Candidats $cdt)
	{
		$cdt->cdt_status = 'ET';
		$cdt->save();

		return back();
	}

	public function integration(Candidats $cdt, Request $request)
	{
		$cdt->cdt_status = $request->integration;
		$cdt->save();

		return back();
	}

	public function integrer(Candidats $cdt, Request $request)
	{
		$cdt->cdt_status = 'INT';
		$cdt->save();

		return back();
	}

	public function demissionner(Candidats $cdt, Request $request)
	{
		$cdt->cdt_status = 'DEM';
		$cdt->save();

		return back();
	}

	public function ajouter_candidat(Request $request)
	{
		$this->validate(request(), [
			'type' => 'required|min:1',
			'civilite' => 'required',
			'nom' => 'required|string|max:255',
			'prenom' => 'required|string|max:255',
			'date_naiss' => 'required|date_format:Y-m-d',
			'ine' => 'string|min:11|max:11|nullable',
		]);

		$candidat = new Candidats;
		$candidat->cdt_type = $request->type;
		$candidat->cdt_nom = $request->nom;
		$candidat->cdt_nom_de_naissance = $request->nom_naiss;
		$candidat->cdt_prenom = $request->prenom;
		$candidat->cdt_date_de_naissance = $request->date_naiss;
		$candidat->cdt_adresse = $request->adresse;
		$candidat->cdt_cp = $request->cp;
		$candidat->cdt_ville = $request->comunne;
		$candidat->cdt_pays = $request->pays;
		$candidat->cdt_tel_fixe = $request->tel_fix;
		$candidat->cdt_tel_mob = $request->mobile;
		$candidat->cdt_email = $request->email;
		$candidat->cdt_mention_bac = $request->mention;

		$candidat->cdt_postbac_etablissement = $request->form_type;
		$candidat->cdt_postbac_intitule = $request->form_intitule;
		$candidat->cdt_postbac_etablissement = $request->form_lieu;

		$candidat->cdt_diplome_etablissement = $request->diplome_etab_type;
		$candidat->cdt_diplome_type = $request->diplome_type;
		$candidat->cdt_diplome_intitule = $request->diplome_intitule;
		$candidat->cdt_code_ine_bea = $request->ine;

		$candidat->cdt_parcours = $request->parcours;
		$candidat->cdt_position_voeu = $request->position;
		$candidat->cdt_note_ang_bac = $request->note_ang_bac;
		$candidat->cdt_note_ang_univ = $request->note_ang_univ;
		$candidat->cdt_toeic = $request->toeic;
		$candidat->cdt_score_eng = $request->score;

		$candidat->save();


		return back()->with('message', 'Vous avez bien ajouté un candidat');
	}

	public function modifier_candidat(Request $request, Candidats $cdt)
	{
		$this->validate(request(), [
			'type' => 'required|min:1',
			'civilite' => 'required',
			'nom' => 'required|string|max:255',
			'prenom' => 'required|string|max:255',
			'date_naiss' => 'required|date_format:Y-m-d',
			'ine' => 'string|min:11|max:11|nullable',
		]);

		$candidat = $cdt ;
		$candidat->cdt_type = $request->type;
		$candidat->cdt_nom = $request->nom;
		$candidat->cdt_nom_de_naissance = $request->nom_naiss;
		$candidat->cdt_prenom = $request->prenom;
		$candidat->cdt_date_de_naissance = $request->date_naiss;
		$candidat->cdt_adresse = $request->adresse;
		$candidat->cdt_cp = $request->cp;
		$candidat->cdt_ville = $request->comunne;
		$candidat->cdt_pays = $request->pays;
		$candidat->cdt_tel_fixe = $request->tel_fix;
		$candidat->cdt_tel_mob = $request->mobile;
		$candidat->cdt_email = $request->email;
		$candidat->cdt_mention_bac = $request->mention;

		$candidat->cdt_postbac_etablissement = $request->form_type;
		$candidat->cdt_postbac_intitule = $request->form_intitule;
		$candidat->cdt_postbac_etablissement = $request->form_lieu;

		$candidat->cdt_diplome_etablissement = $request->diplome_etab_type;
		$candidat->cdt_diplome_type = $request->diplome_type;
		$candidat->cdt_diplome_intitule = $request->diplome_intitule;

		$candidat->cdt_parcours = $request->parcours;
		$candidat->cdt_position_voeu = $request->position;
		$candidat->cdt_note_ang_bac = $request->note_ang_bac;
		$candidat->cdt_note_ang_univ = $request->note_ang_univ;
		$candidat->cdt_toeic = $request->toeic;
		$candidat->cdt_score_eng = $request->score;

		$candidat->save();


		return back()->with('message', 'Vous avez bien ajouté un candidat');
	}

	public function archiver(Request $request)
	{
		$this->validate(request(), [
			'year' => 'required|digits:4',
		]);

		$cdts = Candidats::get()->toArray();

		try {
			foreach ($cdts as $cdt) {
				$cdt['cdt_id'] = $request->year."_".$cdt['cdt_id'];

				$archive = Archive::updateOrCreate($cdt, ['cdt_year' => $request->year]);
			}

		} catch (\Exception $e) {
			return back()->withErrors($e->errorInfo[2]);
		}
		return back()->with('message', 'Vous avez bien archivé les données');
	}

	public function export() 
	{
		ob_end_clean(); ob_start();
		Excel::create('Inscriptions_ESIAB', function($excel) {

			$excel->sheet('Candidats', function($sheet) {

				$sheet->setOrientation('portrait');
				$sheet->setPageMargin(array(
					0.25, 0.30, 0.25, 0.30
				));

				$sheet->setStyle(array(
					'font' => array(
						'name' => 'Calibri',
						'size' => 15
					)
				));

				$k = Candidats::count()+1;
				$sheet->fromArray(Candidats::all());
				while($k > 0){
					$sheet->row($k)->setHeight($k, 25);
					$k--;
				}

				$sheet->setWidth(array(
					'A' => 5,
					'B' => 20
				));
			});

			$excel->sheet('Archive', function($sheet) {

				$sheet->setOrientation('portrait');
				$sheet->setPageMargin(array(
					0.25, 0.30, 0.25, 0.30
				));

				$sheet->setStyle(array(
					'font' => array(
						'name' => 'Calibri',
						'size' => 15
					)
				));

				$k = Archive::count()+1;
				$sheet->fromArray(Archive::all());
				while($k > 0){
					$sheet->row($k)->setHeight($k, 25);
					$k--;
				}

				$sheet->setWidth(array(
					'A' => 5,
					'B' => 20
				));
			});

		})->download('xls');
	}

	public function add_note_dossier(Request $r, Candidats $cdt)
	{
		$this->validate(request(), [
			'note_dos' => 'required|string|max:2',
			'nom_jury1' => 'required|string|max:20',
			'nom_jury2' => 'required|string|max:20',
		]);

		$cdt->cdt_note_dossier = $r->note_dos;
		$cdt->cdt_jury_dossier1 = $r->nom_jury1;
		$cdt->cdt_jury_dossier2 = $r->nom_jury2;
		$cdt->save();

		return back()->with('message', 'Vous avez bien changé la note de dossier');
	}

	public function add_note_entretien(Request $r, Candidats $cdt)
	{
		$this->validate(request(), [
			'note_ent' => 'required|string|max:2',
			'nom_jury_ent1' => 'required|string|max:20',
			'nom_jury_ent2' => 'required|string|max:20',
			'nom_jury_ent3' => 'required|string|max:20',
		]);

		$cdt->cdt_note_entretien = $r->note_ent;
		$cdt->cdt_jury_entretien1 = $r->nom_jury_ent1;
		$cdt->cdt_jury_entretien2 = $r->nom_jury_ent2;
		$cdt->cdt_jury_entretien3 = $r->nom_jury_ent3;
		$cdt->save();

		return back()->with('message', 'Vous avez bien changé la note de l\'entretien');
	}

	public function supprimer_candidats()
	{
		$cdt = Candidats::truncate();

		return back()->with('message', 'Vous avez bien supprimé tout les candidats');
	}

	public function delete_year($year)
	{
		$cdt = Archive::where('cdt_year', $year)->delete();

		return back()->with('message', 'Vous avez bien supprimé tout les candidats');
	}
}