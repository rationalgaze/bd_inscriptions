<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
	protected $table = 'archive';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

}
