<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidats extends Model
{

	protected $table = 'candidats';
	protected $primaryKey = 'cdt_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];
}
