<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth/login');
});

Auth::routes();

Route::prefix('candidats')->group(function(){

	// candidats
	Route::get('/', 'CandidatsController@index')->name('candidats.dashboard');
	Route::post('/change_pass', 'CandidatsController@change_pass')->name('change_pass');
	Route::post('/import_ciel2', 'CandidatsController@importExcel')->name('import_ciel2');
	Route::post('/ajouter_candidat', 'CandidatsController@ajouter_candidat')->name('ajouter_candidat');
	Route::post('/modifier_candidat', 'CandidatsController@modifier_candidat')->name('modifier_candidat');
	Route::get('/show/{cdt}', 'CandidatsController@show')->name('show');
	Route::post('/decline/{cdt}', 'CandidatsController@decline')->name('decline');
	Route::post('/accept/{cdt}', 'CandidatsController@accept')->name('accept');
	Route::post('/lc/{cdt}', 'CandidatsController@listecomp')->name('lc');
	Route::post('/et/{cdt}', 'CandidatsController@entretien')->name('et');
	Route::post('/int/{cdt}', 'CandidatsController@integration')->name('int');
	Route::post('/integrer/{cdt}', 'CandidatsController@integrer')->name('integrer');
	Route::post('/dem/{cdt}', 'CandidatsController@demissionner')->name('dem');
	Route::post('/archive', 'CandidatsController@archiver')->name('archive');
	Route::get('/export', 'CandidatsController@export')->name('export');

	Route::post('/add_note_dossier/{cdt}', 'CandidatsController@add_note_dossier')->name('add_note_dossier');
	Route::post('/add_note_entretien/{cdt}', 'CandidatsController@add_note_entretien')->name('add_note_entretien');
	Route::post('/supprimer_candidats', 'CandidatsController@supprimer_candidats')->name('supprimer_candidats');
	Route::post('/delete_year/{year}', 'CandidatsController@delete_year')->name('delete_year');

});

