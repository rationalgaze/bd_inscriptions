
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});

$(function () {
	$('[data-tooltip="true"]').tooltip();
});

$('#modChange_status').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var recipient = button.data('chg'); // Extract info from data-* attributes

	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	var modal = $(this);
	modal.find('#decline-mdl-fm').attr('action', '/candidats/decline/' + recipient);
	modal.find('#accept-mdl-fm').attr('action', '/candidats/accept/' + recipient);
	modal.find('#lc-mdl-fm').attr('action', '/candidats/lc/' + recipient);
	modal.find('#et-mdl-fm').attr('action', '/candidats/et/' + recipient);
	modal.find('#int-mdl-fm').attr('action', '/candidats/int/' + recipient);
});

$('#modAdd_note_d').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var recipient = button.data('note_dossier'); // Extract info from data-* attributes

	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	var modal = $(this);
	modal.find('#fm_note_doss').attr('action', '/add_note_dossier/' + recipient);
});

$('#modAdd_note_e').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var recipient = button.data('note_entretien'); // Extract info from data-* attributes

	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	var modal = $(this);
	modal.find('#fm_note_ent').attr('action', '/add_note_entretien/' + recipient);
});

$("#toeic").on('change', function(){
	if($(this).val() != ''){
		$("#score-hidden").removeClass('hidden');
	} else {
		$("#score-hidden").addClass('hidden');
	}
})

$('#modDel_from_archive').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var recipient = button.data('year'); // Extract info from data-* attributes

	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	var modal = $(this);
	modal.find('#del_year').attr('action', '/candidats/delete_year/' + recipient);
});