<tr class="success">
	<td class="table-text text-center">
		<div>{{ $i++ }}</div>
	</td>
	<td class="table-text">
		<div>
			<a href="{{ route('show', $cdt->cdt_id) }}" class="">
				{{ $cdt->cdt_civilite." ".$cdt->cdt_nom }}
			</a>
		</div>
		<div>{{ $cdt->cdt_prenom }}</div>
		<div><em><small>{{ Carbon\Carbon::parse($cdt->cdt_date_de_naissance)->format('d-m-Y') }}</small></em></div>
	</td>
	<td class="table-text">
		<div><em>{{ $cdt->cdt_diplome_intitule }}</em> - {{ $cdt->cdt_diplome_type }}</div>
		<div><strong>{{ $cdt->cdt_diplome_etablissement }}</strong></div>
	</td>
	<td class="table-text">
		<div><strong>{{ $cdt->cdt_parcours }}</strong></div>
	</td>
	<td>
		@if($cdt->cdt_mention_bac == 'P')
			<div>Passable</div>
		@elseif($cdt->cdt_mention_bac == 'B')
			<div>Bien</div>
		@elseif($cdt->cdt_mention_bac == 'AB')
			<div>Assez Bien</div>
		@elseif($cdt->cdt_mention_bac == 'TB')
			<div>Très Bien</div>
		@endif
	</td>
	<td>
		@if($cdt->cdt_note_ang_bac)<div><u>BAC</u> : {{ $cdt->cdt_note_ang_bac }}</div>@endif
		@if($cdt->cdt_note_ang_univ)<div><u>Superieur</u> : {{ $cdt->cdt_note_ang_univ }}</div>@endif
		@if($cdt->cdt_toeic || $cdt->cdt_score_eng)
			<div><u>Certificat</u> : {{ $cdt->cdt_toeic }} {{ $cdt->cdt_score_eng }}</div>
		@endif
	</td>
	<td>
		<div>
			@if($cdt->cdt_note_dossier) 
				<strong>D : {{ $cdt->cdt_note_dossier }}</strong>
			@endif
		</div>
		<div>
			@if($cdt->cdt_note_entretien) 
				<strong>E : {{ $cdt->cdt_note_entretien }}</strong>
			@endif
		</div>
	</td>
	<td class="table-text text-center">
		<div>
			<i class="material-icons md-24 text-danger">close</i>
		</div>
		<div>
			<a href="#" data-toggle="modal" data-target="#modChange_status" data-chg="{{ $cdt->cdt_id }}" class="text-danger text-uppercase">
				<strong>Démissionné</strong>
			</a>
		</div>
	</td>
</tr>