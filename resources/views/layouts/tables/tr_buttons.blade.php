<tr>
	<td class="table-text text-center">
		<div>{{ $i++ }}</div>
	</td>
	<td class="table-text">
		<div>
			<a href="{{ route('show', $cdt->cdt_id) }}" class="">
				{{ $cdt->cdt_civilite." ".$cdt->cdt_nom }}
			</a>
		</div>
		<div>{{ $cdt->cdt_prenom }}</div>
		<div><em><small>{{ Carbon\Carbon::parse($cdt->cdt_date_de_naissance)->format('d-m-Y') }}</small></em></div>
	</td>
	<td class="table-text">
		<div><em>{{ $cdt->cdt_diplome_intitule }}</em> - {{ $cdt->cdt_diplome_type }}</div>
		<div><strong>{{ $cdt->cdt_diplome_etablissement }}</strong></div>
	</td>
	<td class="table-text">
		@if($cdt->cdt_parcours == 'DUTL2')
			<div><strong>DUT + L2</strong></div>
		@elseif($cdt->cdt_parcours == 'DUTL3')
			<div><strong>DUT + L3</strong></div>
		@elseif($cdt->cdt_parcours == 'DUTP')
			<div><strong>DUT + Prépa</strong></div>
		@elseif($cdt->cdt_parcours == 'BTSP')
			<div><strong>BTS + Prépa</strong></div>
		@else
			<div><strong>{{ $cdt->cdt_parcours }}</strong></div>
		@endif
	</td>
	<td>
		@if($cdt->cdt_mention_bac == 'P')
			<div>Passable</div>
		@elseif($cdt->cdt_mention_bac == 'B')
			<div>Bien</div>
		@elseif($cdt->cdt_mention_bac == 'AB')
			<div>Assez Bien</div>
		@elseif($cdt->cdt_mention_bac == 'TB')
			<div>Très Bien</div>
		@endif
	</td>
	<td>
		@if($cdt->cdt_note_ang_bac)<div><u>BAC</u> : {{ $cdt->cdt_note_ang_bac }}</div>@endif
		@if($cdt->cdt_note_ang_univ)<div><u>Superieur</u> : {{ $cdt->cdt_note_ang_univ }}</div>@endif
		@if($cdt->cdt_toeic || $cdt->cdt_score_eng)
			<div><u>Certificat</u> : {{ $cdt->cdt_toeic }} {{ $cdt->cdt_score_eng }}</div>
		@endif
	</td>
	<td>
		<div>
			@if($cdt->cdt_note_dossier) 
				<strong>D : {{ $cdt->cdt_note_dossier }}</strong>
			@endif
		</div>
		<div>
			@if($cdt->cdt_note_entretien) 
				<strong>E : {{ $cdt->cdt_note_entretien }}</strong>
			@endif
		</div>
	</td>
	<td>
		<div class="text-center">
			<form class="status-fm" action="{{ route('decline', $cdt->cdt_id) }}" method="POST">
				{{ csrf_field() }}
				<button type="submit" class="btn btn-danger btn-xs">
					<i class="material-icons md-18">close</i>
				</button>
			</form>
			<form class="status-fm" action="{{ route('accept', $cdt->cdt_id) }}" method="POST">
				{{ csrf_field() }}
				<button type="submit" class="btn btn-success btn-xs">
					<i class="material-icons md-18">check</i>
				</button>
			</form>
			<form class="status-fm" action="{{ route('lc', $cdt->cdt_id) }}" method="POST">
				{{ csrf_field() }}
				<button type="submit" class="btn btn-warning btn-xs">
					<strong>LC</strong>
				</button>
			</form>
			<form class="status-fm" action="{{ route('et', $cdt->cdt_id) }}" method="POST">
				{{ csrf_field() }}
				<button type="submit" class="btn btn-info btn-xs">
					<i class="material-icons md-18">forum</i>
				</button>
			</form>
		</div>
	</td>
</tr>