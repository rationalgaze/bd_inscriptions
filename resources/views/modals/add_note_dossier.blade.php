<!-- Modal Ajouter un Candidat -->
<div class="modal fade" id="modAdd_note_d" tabindex="-1" role="dialog" aria-labelledby="Modify Mdp">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Ajouter la note pour ce dossier</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12">
						<form id="fm_note_doss" role="form" method="POST" action="{{ route('add_note_dossier', $cdt) }}">
							{{ csrf_field() }}

							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

							<div class="form-group{{ $errors->has('note_dos') ? ' has-error' : '' }} required">
								<label for="note_dos" class="control-label">Note</label>

								<input id="note_dos" type="text" class="form-control" name="note_dos" placeholder="Note ex: 3-" value="{{ $cdt->cdt_note_dossier }}" autofocus required>

								@if ($errors->has('note_dos'))
									<span class="help-block">
										<strong>{{ $errors->first('note_dos') }}</strong>
									</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('nom_jury1') ? ' has-error' : '' }} required">
								<label for="nom_jury1" class="control-label">Nom du membre de jury 1</label>

								<input id="nom_jury1" type="text" class="form-control" name="nom_jury1" placeholder="Nom" value="{{ $cdt->cdt_jury_dossier1 }}">

								@if ($errors->has('nom_jury1'))
									<span class="help-block">
										<strong>{{ $errors->first('nom_jury1') }}</strong>
									</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('nom_jury2') ? ' has-error' : '' }} required">
								<label for="nom_jury2" class="control-label">Nom du membre de jury 2</label>

								<input id="nom_jury2" type="text" class="form-control" name="nom_jury2" placeholder="Nom" value="{{ $cdt->cdt_jury_dossier2 }}">

								@if ($errors->has('nom_jury2'))
									<span class="help-block">
										<strong>{{ $errors->first('nom_jury2') }}</strong>
									</span>
								@endif
							</div>

							<div class="form-group required">
								<label for="nom_jury2" class="control-label">Champs obligatoires</label>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary">
										Enregistrer
									</button>
									<button class="btn btn-danger pull-right" data-dismiss="modal" aria-label="Close">
										Retour
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>