<!-- Modal Ajouter un Candidat -->
<div class="modal fade" id="modAdd_candidat" tabindex="-1" role="dialog" aria-labelledby="Ajouter un candidat">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Ajouter des données pour un candidat</strong></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="{{ route('ajouter_candidat') }}">
					{{ csrf_field() }}

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<fieldset class="personal col-xs-6">
						<h4 class="text-primary"><strong>Information personnelle :</strong></h4>
						<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }} required">
							<label for="type" class="col-md-5 col-sm-5 control-label">Type de dossiers</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('type', 
									[
										'' => 'Selectionnez le type de dossiers',
										'E' => 'Étrangère et papiers',
										'P' => 'Class prepa',
										'C' => 'Ciell2',
									], null, ['class' => 'form-control']) !!}

								@if ($errors->has('type'))
									<span class="help-block">
										<strong>{{ $errors->first('type') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('civilite') ? ' has-error' : '' }} required">
							<label for="name" class="col-md-5 col-sm-5 control-label">Civilité</label>

							<div class="col-md-6 col-sm-6">
								<label class="radio-inline"><input type="radio" name="civilite" value="M">Monsieur</label>
								<label class="radio-inline"><input type="radio" name="civilite" value="MME">Madame</label>
								@if ($errors->has('civilite'))
									<span class="help-block">
										<strong>{{ $errors->first('civilite') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('nom') ? ' has-error' : '' }} required">
							<label for="nom" class="col-md-5 col-sm-5 control-label">Nom</label>

							<div class="col-md-6 col-sm-6">
								<input id="nom" type="text" class="form-control" name="nom" placeholder="Nom" autofocus required>

								@if ($errors->has('nom'))
									<span class="help-block">
										<strong>{{ $errors->first('nom') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('nom_naiss') ? ' has-error' : '' }}">
							<label for="nom_naiss" class="col-md-5 col-sm-5 control-label">Nom d'usage ou marital</label>

							<div class="col-md-6 col-sm-6">
								<input id="nom_naiss" type="text" class="form-control" name="nom_naiss" placeholder="Nom de naissance" autofocus>

								@if ($errors->has('nom_naiss'))
									<span class="help-block">
										<strong>{{ $errors->first('nom_naiss') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }} required">
							<label for="prenom" class="col-md-5 col-sm-5 control-label">Prénom</label>

							<div class="col-md-6 col-sm-6">
								<input id="prenom" type="text" class="form-control" name="prenom" placeholder="Prenom" required>

								@if ($errors->has('prenom'))
									<span class="help-block">
										<strong>{{ $errors->first('prenom') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('date_naiss') ? ' has-error' : '' }} required">
							<label for="date_naiss" class="col-md-5 col-sm-5 control-label">Date de naissance</label>

							<div class="col-md-6 col-sm-6">
								<input id="date_naiss" type="date" class="form-control" name="date_naiss" placeholder="Ex : 2000-01-06" required>

								@if ($errors->has('date_naiss'))
									<span class="help-block">
										<strong>{{ $errors->first('date_naiss') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('adresse') ? ' has-error' : '' }} required">
							<label for="adresse" class="col-md-5 col-sm-5 control-label">Adresse du candidat</label>

							<div class="col-md-6 col-sm-6">
								<input id="adresse" type="text" class="form-control" name="adresse" placeholder="Votre adresse" required>

								@if ($errors->has('adresse'))
									<span class="help-block">
										<strong>{{ $errors->first('adresse') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('cp') ? ' has-error' : '' }} required">
							<label for="cp" class="col-md-5 col-sm-5 control-label">Code postal</label>

							<div class="col-md-6 col-sm-6">
								<input id="cp" type="text" class="form-control" name="cp" placeholder="Votre Code postal" required>

								@if ($errors->has('cp'))
									<span class="help-block">
										<strong>{{ $errors->first('cp') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('comunne') ? ' has-error' : '' }} required">
							<label for="comunne" class="col-md-5 col-sm-5 control-label">Commune</label>

							<div class="col-md-6 col-sm-6">
								<input id="comunne" type="text" class="form-control" name="comunne" placeholder="Ex: Brest" required>

								@if ($errors->has('comunne'))
									<span class="help-block">
										<strong>{{ $errors->first('comunne') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('pays') ? ' has-error' : '' }} required">
							<label for="pays" class="col-md-5 col-sm-5 control-label" required>Pays</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('pays', 
									[
										'' =>'Selectionnez un pays',
										'France' =>'France',
										'Afghanistan'=>'Afghanistan',
										'Afrique_Centrale'=>'Afrique_Centrale',
										'Afrique_du_sud'=>'Afrique du Sud',
										'Albanie'=>'Albanie',
										'Algerie'=>'Algerie',
										'Allemagne'=>'Allemagne',
										'Andorre'=>'Andorre',
										'Angola'=>'Angola',
										'Anguilla'=>'Anguilla',
										'Arabie_Saoudite'=>'Arabie Saoudite',
										'Argentine'=>'Argentine',
										'Armenie'=>'Armenie',
										'Australie'=>'Australie',
										'Autriche'=>'Autriche',
										'Azerbaidjan'=>'Azerbaidjan',
										'Bahamas'=>'Bahamas',
										'Bangladesh'=>'Bangladesh',
										'Barbade'=>'Barbade',
										'Bahrein'=>'Bahrein',
										'Belgique'=>'Belgique',
										'Belize'=>'Belize',
										'Benin'=>'Benin',
										'Bermudes'=>'Bermudes',
										'Bielorussie'=>'Bielorussie',
										'Bolivie'=>'Bolivie',
										'Botswana'=>'Botswana',
										'Bhoutan'=>'Bhoutan',
										'Boznie_Herzegovine'=>'Boznie Herzegovine',
										'Bresil'=>'Bresil',
										'Brunei'=>'Brunei',
										'Bulgarie'=>'Bulgarie',
										'Burkina_Faso'=>'Burkina Faso',
										'Burundi'=>'Burundi',
										'Caiman'=>'Caiman',
										'Cambodge'=>'Cambodge',
										'Cameroun'=>'Cameroun',
										'Canada'=>'Canada',
										'Canaries'=>'Canaries',
										'Cap_vert'=>'Cap Vert',
										'Chili'=>'Chili',
										'Chine'=>'Chine',
										'Chypre'=>'Chypre',
										'Colombie'=>'Colombie',
										'Comores'=>'Colombie',
										'Congo'=>'Congo',
										'Congo_democratique'=>'Congo democratique',
										'Cook'=>'Cook',
										'Coree_du_Nord'=>'Coree du Nord',
										'Coree_du_Sud'=>'Coree du Sud',
										'Costa_Rica'=>'Costa Rica',
										'Cote_d_Ivoire'=>'Côte d\'Ivoire',
										'Croatie'=>'Croatie',
										'Cuba'=>'Cuba',
										'Danemark'=>'Danemark',
										'Djibouti'=>'Djibouti',
										'Dominique'=>'Dominique',
										'Egypte'=>'Egypte',
										'Emirats_Arabes_Unis'=>'Emirats Arabes Unis',
										'Equateur'=>'Equateur',
										'Erythree'=>'Erythree',
										'Espagne'=>'Espagne',
										'Estonie'=>'Estonie',
										'Etats_Unis'=>'Etats Unis',
										'Ethiopie'=>'Ethiopie',
										'Falkland'=>'Falkland',
										'Feroe'=>'Feroe',
										'Fidji'=>'Fidji',
										'Finlande'=>'Finlande',
										'France'=>'France',
										'Gabon'=>'Gabon',
										'Gambie'=>'Gambie',
										'Georgie'=>'Georgie',
										'Ghana'=>'Ghana',
										'Gibraltar'=>'Gibraltar',
										'Grece'=>'Grece',
										'Grenade'=>'Grenade',
										'Groenland'=>'Groenland',
										'Guadeloupe'=>'Guadeloupe',
										'Guam'=>'Guam',
										'Guatemala'=>'Guatemala',
										'Guernesey'=>'Guernesey',
										'Guinee'=>'Guinee',
										'Guinee_Bissau'=>'Guinee Bissau',
										'Guinee equatoriale'=>'Guinee Equatoriale',
										'Guyana'=>'Guyana',
										'Guyane_Francaise'=>'Guyane Francaise',
										'Haiti'=>'Haiti',
										'Hawaii'=>'Hawaii',
										'Honduras'=>'Honduras',
										'Hong_Kong'=>'Hong Kong',
										'Hongrie'=>'Hongrie',
										'Inde'=>'Inde',
										'Indonesie'=>'Indonesie',
										'Iran'=>'Iran',
										'Iraq'=>'Iraq',
										'Irlande'=>'Irlande',
										'Islande'=>'Islande',
										'Israel'=>'Israel',
										'Italie'=>'italie',
										'Jamaique'=>'Jamaique',
										'Jan Mayen'=>'Jan Mayen',
										'Japon'=>'Japon',
										'Jersey'=>'Jersey',
										'Jordanie'=>'Jordanie',
										'Kazakhstan'=>'Kazakhstan',
										'Kenya'=>'Kenya',
										'Kirghizstan'=>'Kirghizistan',
										'Kiribati'=>'Kiribati',
										'Koweit'=>'Koweit',
										'Laos'=>'Laos',
										'Lesotho'=>'Lesotho',
										'Lettonie'=>'Lettonie',
										'Liban'=>'Liban',
										'Liberia'=>'Liberia',
										'Liechtenstein'=>'Liechtenstein',
										'Lituanie'=>'Lituanie',
										'Luxembourg'=>'Luxembourg',
										'Lybie'=>'Lybie',
										'Macao'=>'Macao',
										'Macedoine'=>'Macedoine',
										'Madagascar'=>'Madagascar',
										'Madère'=>'Madère',
										'Malaisie'=>'Malaisie',
										'Malawi'=>'Malawi',
										'Maldives'=>'Maldives',
										'Mali'=>'Mali',
										'Malte'=>'Malte',
										'Man'=>'Man',
										'Mariannes du Nord'=>'Mariannes du Nord',
										'Maroc'=>'Maroc',
										'Marshall'=>'Marshall',
										'Martinique'=>'Martinique',
										'Maurice'=>'Maurice',
										'Mauritanie'=>'Mauritanie',
										'Mayotte'=>'Mayotte',
										'Mexique'=>'Mexique',
										'Micronesie'=>'Micronesie',
										'Midway'=>'Midway',
										'Moldavie'=>'Moldavie',
										'Monaco'=>'Monaco',
										'Mongolie'=>'Mongolie',
										'Montserrat'=>'Montserrat',
										'Mozambique'=>'Mozambique',
										'Namibie'=>'Namibie',
										'Nauru'=>'Nauru',
										'Nepal'=>'Nepal',
										'Nicaragua'=>'Nicaragua',
										'Niger'=>'Niger',
										'Nigeria'=>'Nigeria',
										'Niue'=>'Niue',
										'Norfolk'=>'Norfolk',
										'Norvege'=>'Norvege',
										'Nouvelle_Caledonie'=>'Nouvelle Caledonie',
										'Nouvelle_Zelande'=>'Nouvelle Zelande',
										'Oman'=>'Oman',
										'Ouganda'=>'Ouganda',
										'Ouzbekistan'=>'Ouzbekistan',
										'Pakistan'=>'Pakistan',
										'Palau'=>'Palau',
										'Palestine'=>'Palestine',
										'Panama'=>'Panama',
										'Papouasie_Nouvelle_Guinee'=>'Papouasie Nouvelle Guinee',
										'Paraguay'=>'Paraguay',
										'Pays_Bas'=>'Pays_Bas',
										'Perou'=>'Perou',
										'Philippines'=>'Philippines',
										'Pologne'=>'Pologne',
										'Polynesie'=>'Polynesie',
										'Porto_Rico'=>'Porto Rico',
										'Portugal'=>'Portugal',
										'Qatar'=>'Qatar',
										'Republique_Dominicaine'=>'Republique Dominicaine',
										'Republique_Tcheque'=>'Republique Tcheque',
										'Reunion'=>'Reunion',
										'Roumanie'=>'Roumanie',
										'Royaume_Uni'=>'Royaume Uni',
										'Russie'=>'Russie',
										'Rwanda'=>'Rwanda',
										'Sahara Occidental'=>'Sahara Occidental',
										'Sainte_Lucie'=>'Sainte Lucie',
										'Saint_Marin'=>'Saint Marin',
										'Salomon'=>'Salomon',
										'Salvador'=>'Salvador',
										'Samoa_Occidentales'=>'Samoa Occidentales',
										'Samoa_Americaine'=>'Samoa Americaine',
										'Sao_Tome_et_Principe'=>'Sao Tome et Principe',
										'Senegal'=>'Senegal',
										'Seychelles'=>'Seychelles',
										'Sierra Leone'=>'Sierra Leone',
										'Singapour'=>'Singapour',
										'Slovaquie'=>'Slovaquie',
										'Slovenie'=>'Slovenie',
										'Somalie'=>'Somalie',
										'Soudan'=>'Soudan',
										'Sri_Lanka'=>'Sri Lanka',
										'Suede'=>'Suede',
										'Suisse'=>'Suisse',
										'Surinam'=>'Surinam',
										'Swaziland'=>'Swaziland',
										'Syrie'=>'Syrie',
										'Tadjikistan'=>'Tadjikistan',
										'Taiwan'=>'Taiwan',
										'Tonga'=>'Tonga',
										'Tanzanie'=>'Tanzanie',
										'Tchad'=>'Tchad',
										'Thailande'=>'Thailande',
										'Tibet'=>'Tibet',
										'Timor_Oriental'=>'Timor Oriental',
										'Togo'=>'Togo',
										'Trinite_et_Tobago'=>'Trinite et Tobago',
										'Tristan da cunha'=>'Tristan da cunha',
										'Tunisie'=>'Tunisie',
										'Turkmenistan'=>'Turmenistan',
										'Turquie'=>'Turquie',
										'Ukraine'=>'Ukraine',
										'Uruguay'=>'Uruguay',
										'Vanuatu'=>'Vanuatu',
										'Vatican'=>'Vatican',
										'Venezuela'=>'Venezuela',
										'Vierges_Americaines'=>'Vierges Americaines',
										'Vierges_Britanniques'=>'Vierges Britanniques',
										'Vietnam'=>'Vietnam',
										'Wake'=>'Wake',
										'Wallis et Futuma'=>'Wallis et Futuma',
										'Yemen'=>'Yemen',
										'Yougoslavie'=>'Yougoslavie',
										'Zambie'=>'Zambie',
										'Zimbabwe'=>'Zimbabwe',
									], 
									null, ['class' => 'form-control']) !!}

								@if ($errors->has('pays'))
									<span class="help-block">
										<strong>{{ $errors->first('pays') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('tel_fix') ? ' has-error' : '' }}">
							<label for="tel_fix" class="col-md-5 col-sm-5 control-label">Téléphone </label>

							<div class="col-md-6 col-sm-6">
								<input id="tel_fix" type="tel" class="form-control" name="tel_fix" placeholder="Ex: 0199887766">

								@if ($errors->has('tel_fix'))
									<span class="help-block">
										<strong>{{ $errors->first('tel_fix') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
							<label for="mobile" class="col-md-5 col-sm-5 control-label">Portable</label>

							<div class="col-md-6 col-sm-6">
								<input id="mobile" type="tel" class="form-control" name="mobile" placeholder="Ex: 0199887766">

								@if ($errors->has('mobile'))
									<span class="help-block">
										<strong>{{ $errors->first('mobile') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} required">
							<label for="email" class="col-md-5 col-sm-5 control-label">E-mail</label>

							<div class="col-md-6 col-sm-6">
								<input id="email" type="email" class="form-control" name="email" placeholder="votrenom@host.fr" required>

								@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('mention') ? ' has-error' : '' }} required">
							<label for="mention" class="col-md-5 col-sm-5 control-label">Mention au BAC</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('mention', 
									[
										'' => 'Selectionnez votre mention',
										'P' => 'Passable - ou sans mention',
										'AB' => 'Assez Bien',
										'B' => 'Bien',
										'TB' => 'Très Bien',
									], null, ['class' => 'form-control']) !!}

								@if ($errors->has('mention'))
									<span class="help-block">
										<strong>{{ $errors->first('mention') }}</strong>
									</span>
								@endif
							</div>
						</div>
					</fieldset>

					<fieldset class="formations col-xs-6">


						<h4 class="text-primary"><strong>Dernière formation suivie :</strong></h4>

						<div class="form-group{{ $errors->has('form_type') ? ' has-error' : '' }} required">
							<label for="form_type" class="col-md-5 col-sm-5 control-label">Formation type</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('form_type', 
									[
										'' => 'Autre',
										'B.T.S.' => 'B.T.S.',
										'BTS' => 'BTS',
										'CP' => 'Classes Préparatoires',
										'D.U.T.' => 'D.U.T.',
										'DUT' => 'DUT',
										'Fac_Lettres_Sciences' => 'Faculté de Lettres et Sciences humaines',
										'MED' => 'Faculté de Médecine',
										'SESS' => 'Faculté de sciences de L\'éducation et sciences sociales',
										'ST' => 'Faculté des Sciences et Techniques',
										'STAPS' => 'Faculté des Sciences et Techniques des Activités Physiques et Sportives',
										'I.U.P.' => 'I.U.P.',
										'L1' => 'Licence 1',
										'L2' => 'Licence 2',
										'L3' => 'Licence 3',
										'LPRO' => 'Licence Professionnelle',
										'M1' => 'Master1',
										'M2' => 'Master2',
									], null, ['class' => 'form-control']) !!}

								@if ($errors->has('form_type'))
									<span class="help-block">
										<strong>{{ $errors->first('form_type') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('form_intitule') ? ' has-error' : '' }} required">
							<label for="form_intitule" class="col-md-5 col-sm-5 control-label">Formation intitulé</label>

							<div class="col-md-6 col-sm-6">
								<input id="form_intitule" type="text" class="form-control" name="form_intitule" placeholder="Formation intitulé" required>

								@if ($errors->has('form_intitule'))
									<span class="help-block">
										<strong>{{ $errors->first('form_intitule') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('form_lieu') ? ' has-error' : '' }} required">
							<label for="form_lieu" class="col-md-5 col-sm-5 control-label">Formation lieu</label>

							<div class="col-md-6 col-sm-6">
								<input id="form_lieu" type="text" class="form-control" name="form_lieu" placeholder="Ex: Quimper" required>

								@if ($errors->has('form_lieu'))
									<span class="help-block">
										<strong>{{ $errors->first('form_lieu') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<h4 class="text-primary"><strong>Diplôme le plus élevé obtenu après le BAC :</strong></h4>

						<div class="form-group{{ $errors->has('diplome_intitule') ? ' has-error' : '' }}">
							<label for="diplom_intitule" class="col-md-5 col-sm-5 control-label">Diplôme Intitulé</label>

							<div class="col-md-6 col-sm-6">
								<input id="diplom_intitule" type="text" class="form-control" name="diplom_intitule" placeholder="Intitulé">

								@if ($errors->has('diplom_intitule'))
									<span class="help-block">
										<strong>{{ $errors->first('diplom_intitule') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('diplome_etab_type') ? ' has-error' : '' }}">
							<label for="diplome_etab_type" class="col-md-5 col-sm-5 control-label">Diplôme Type d'établissement</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('diplome_etab_type', 
									[
										'Autres' => 'Autre',
										'B.T.S.' => 'B.T.S.',
										'BTS' => 'BTS',
										'Classes Préparatoires' => 'Classes Préparatoires',
										'D.U.T.' => 'D.U.T.',
										'DUT' => 'DUT',
										'Faculté de Lettres et Sciences humaines' => 'Faculté de Lettres et Sciences humaines',
										'Faculté de Médecine' => 'Faculté de Médecine',
										'Faculté de sciences de L\'éducation et sciences sociales' => 'Faculté de sciences de L\'éducation et sciences sociales',
										'Faculté des Sciences et Techniques' => 'Faculté des Sciences et Techniques',
										'Faculté des Sciences et Techniques des Activités Physiques et Sportives' => 'Faculté des Sciences et Techniques des Activités Physiques et Sportives',
										'I.U.P.' => 'I.U.P.',
										'Licence 1' => 'Licence 1',
										'Licence 2' => 'Licence 2',
										'Licence 3' => 'Licence 3',
										'Licence Professionnelle' => 'Licence Professionnelle',
										'Master1' => 'Master1',
										'Master2' => 'Master2',
									], null, ['class' => 'form-control']) !!}
								@if ($errors->has('diplome_etab_type'))
									<span class="help-block">
										<strong>{{ $errors->first('diplome_etab_type') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('diplome_type') ? ' has-error' : '' }}">
							<label for="diplome_type" class="col-md-5 col-sm-5 control-label">Diplôme Type</label>

							<div class="col-md-6 col-sm-6">
								<input id="diplome_type" type="text" class="form-control" name="diplome_type" placeholder="Ex: Communication">

								@if ($errors->has('diplome_type'))
									<span class="help-block">
										<strong>{{ $errors->first('diplome_type') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('ine') ? ' has-error' : '' }}">
							<label for="ine" class="col-md-5 col-sm-5 control-label">Code INE/BEA</label>

							<div class="col-md-6 col-sm-6">
								<input id="ine" type="text" class="form-control" name="ine" placeholder="Note Anglais">

								@if ($errors->has('ine'))
									<span class="help-block">
										<strong>{{ $errors->first('ine') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('parcours') ? ' has-error' : '' }} required">
							<label for="parcours" class="col-md-5 col-sm-5 control-label">Votre parcours</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('parcours', 
									[
										'' => 'Selectionnez votre parcours',
										'DUT' => 'DUT',
										'DUTL2' => 'DUT + L2',
										'DUTL3' => 'DUT + L3',
										'DUTP' => 'DUT + Prépa',
										'L2' => 'L2',
										'L3' => 'L3',
										'M1' => 'M1',
										'BTSP' => 'BTS+prépa',
									], null, ['class' => 'form-control']) !!}
								@if ($errors->has('parcours'))
									<span class="help-block">
										<strong>{{ $errors->first('parcours') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
							<label for="position" class="col-md-5 col-sm-5 control-label">Position dans le liste des vœux</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('position', 
									[
										'' => 'Selectionnez position',
										'1' => '1',
										'2' => '2',
										'3' => '3',
										'4' => '4',
										'5' => '5',
										'6' => '6',
										'7' => '7',
									], null, ['class' => 'form-control']) !!}

								@if ($errors->has('position'))
									<span class="help-block">
										<strong>{{ $errors->first('position') }}</strong>
									</span>
								@endif
							</div>
						</div>


						<div class="form-group{{ $errors->has('note_ang_bac') ? ' has-error' : '' }}">
							<label for="note_ang_bac" class="col-md-5 col-sm-5 control-label">Note en Anglais au Bac</label>

							<div class="col-md-6 col-sm-6">
								<input id="note_ang_bac" type="text" class="form-control" name="note_ang_bac" placeholder="Note sur 20">

								@if ($errors->has('note_ang_bac'))
									<span class="help-block">
										<strong>{{ $errors->first('note_ang_bac') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('note_ang_univ') ? ' has-error' : '' }}">
							<label for="note_ang_univ" class="col-md-5 col-sm-5 control-label">Note en anglais en DUT, BTS, L2, L3</label>

							<div class="col-md-6 col-sm-6">
								<input id="note_ang_univ" type="text" class="form-control" name="note_ang_univ" placeholder="Note sur 20">

								@if ($errors->has('note_ang_univ'))
									<span class="help-block">
										<strong>{{ $errors->first('note_ang_univ') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('toeic') ? ' has-error' : '' }}">
							<label for="toeic" class="col-md-5 col-sm-5 control-label">Certificats en Anglais</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('toeic', 
									[
										'' => 'Selectionnez votre sertificat',
										'TOEIC' => 'TOEIC',
										'TOEFL' => 'TOEFL',
										'CLES' => 'CLES',
										'BULATS' => 'BULATS',
										'CA' => 'Cambridge Advanced',
										'CP' => 'Cambridge Proficiency',
										'IELTS' => 'IELTS',
										'Autre' => 'Autre',
									], null, ['class' => 'form-control', 'id' => 'toeic']) !!}

								@if ($errors->has('toeic'))
									<span class="help-block">
										<strong>{{ $errors->first('toeic') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('score') ? ' has-error' : '' }} hidden" id="score-hidden">
							<label for="score" class="col-md-5 col-sm-5 control-label">Score</label>

							<div class="col-md-6 col-sm-6">
								<input id="score" type="text" class="form-control" name="score">

								@if ($errors->has('score'))
									<span class="help-block">
										<strong>{{ $errors->first('score') }}</strong>
									</span>
								@endif
							</div>
						</div>
					</fieldset>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-5">
							<button type="submit" class="btn btn-primary">
								Enregistrer
							</button>
							<button class="btn btn-danger pull-right" data-dismiss="modal" aria-label="Close">
								Retour
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>