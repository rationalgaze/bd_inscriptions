<!-- Modal Changer Status -->
<div class="modal fade" id="modChange_status" tabindex="-1" role="dialog" aria-labelledby="Modify Mdp">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<p class="modal-title" id="myModalLabel">Changer le staut</p>
			</div>
			<div class="modal-body text-center">
				<form id="decline-mdl-fm" class="status-fm" action="" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="btn btn-danger btn-xs">
						<i class="material-icons md-18">close</i>
					</button>
				</form>
				<form id="accept-mdl-fm" class="status-fm" action="" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="btn btn-success btn-xs">
						<i class="material-icons md-18">check</i>
					</button>
				</form>
				<form id="lc-mdl-fm" class="status-fm" action="" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="btn btn-warning btn-xs">
						<strong>LC</strong>
					</button>
				</form>
				<form id="et-mdl-fm" class="status-fm" action="" method="POST">
					{{ csrf_field() }}
					<button type="submit" class="btn btn-info btn-xs">
						<strong>ET</strong>
					</button>
				</form>
				<div class="row" style="margin: 10px 0 0 0;">
					<div class="col-xs-12">
						<form id="int-mdl-fm" class="status-fm" action="" method="POST">
							{{ csrf_field() }}
							<div class="form-group">
								<label class="radio-inline"><input type="radio" name="integration" value="INT">Integré</label>
								<label class="radio-inline"><input type="radio" name="integration" value="DEM">Demissioné</label>
							</div>
							<button type="submit" class="btn btn-primary btn-sm">
								Valider
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>