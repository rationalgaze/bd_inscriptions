<!-- Modal Ajouter un Candidat -->
<div class="modal fade" id="modDel_from_archive" tabindex="-1" role="dialog" aria-labelledby="Supprimer cette année de l'archive">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Confirmez-vous la suppression ?</strong></h4>
			</div>
			<div class="modal-body text-center">
				<p>Vous allez supprimer les données pour cette année de l'archive.</p>
				<p>Confirmez-vous la suppretion ?</p>
				<form id="del_year" role="form" method="POST" action="{{ route('supprimer_candidats') }}">
					{{ csrf_field() }}
					<button type="submit" class="btn btn-primary">
						Oui
					</button>
					<button class="btn btn-danger" data-dismiss="modal" aria-label="Close">
						Non
					</button>
				</form>
			</div>
		</div>
	</div>
</div>