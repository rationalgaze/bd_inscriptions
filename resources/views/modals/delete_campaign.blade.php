<!-- Modal Ajouter un Candidat -->
<div class="modal fade" id="modDel_campaign" tabindex="-1" role="dialog" aria-labelledby="Supprimer les candidats de cette année d'inscription">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Confirmez-vous la suppression ?</strong></h4>
			</div>
			<div class="modal-body text-center">
				<p>Vous allez supprimer tous les candidats de l'année d'inscription courante.</p>
				<p>Confirmez-vous la suppretion ?</p>
				<form role="form" method="POST" action="{{ route('supprimer_candidats') }}">
					{{ csrf_field() }}
					<button type="submit" class="btn btn-primary">
						Oui
					</button>
					<button class="btn btn-danger" data-dismiss="modal" aria-label="Close">
						Non
					</button>
				</form>
			</div>
		</div>
	</div>
</div>