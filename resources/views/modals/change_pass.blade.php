<!-- Modal Changing Password -->
<div class="modal fade" id="modPass" tabindex="-1" role="dialog" aria-labelledby="Modify Mdp">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><strong>Changement du mot de passe</strong></h4>
			</div>
			<div class="modal-body">
				<form role="form" method="POST" action="{{ route('change_pass') }}">
					{{ csrf_field() }}

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="form-group{{ $errors->has('mdp_act') ? ' has-error' : '' }}">
						<label for="mdp_act" class="control-label">Entrez votre mot de passe actuel</label>

						<input id="mdp_act" type="password" class="form-control" name="mdp_act" required>

						@if ($errors->has('mdp_act'))
							<span class="help-block">
								<strong>{{ $errors->first('mdp_act') }}</strong>
							</span>
						@endif
					</div>

					<div class="form-group{{ $errors->has('mdp_new') ? ' has-error' : '' }}">
						<label for="mdp_new	" class="control-label">Entrez un nouveau mot de passe</label>
						<input id="mdp_new" type="password" class="form-control" name="mdp_new" required>

						@if ($errors->has('mdp_new'))
							<span class="help-block">
								<strong>{{ $errors->first('mdp_new') }}</strong>
							</span>
						@endif
					</div>

					<div class="form-group">
						<label for="mdp_new-confirm" class="control-label">Confirmez votre mot de passe</label>
							<input id="mdp_new-confirm" type="password" class="form-control" name="mdp_new_confirmation" required>
					</div>
					<button type="submit" class="btn btn-primary">Enregistrer</button>
					<button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Retour</button>
				</form>
			</div>
		</div>
	</div>
</div>