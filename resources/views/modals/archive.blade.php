<!-- Modal Import -->
<div class="modal fade" id="modArchive" tabindex="-1" role="dialog" aria-labelledby="Archive">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Archiver les données</strong></h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<form role="form" method="POST" action="{{ route('archive') }}" enctype="multipart/form-data">
							{{ csrf_field() }}

							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

							<div class="form-group required">
								<label for="year">L'année d'inscription</label>
								<input type="num" name="year" id="year" class="form-control" placeholder="1984" required autofocus>
							</div>
							<div class="form-group text-center">
								<button class="btn btn-danger" type="submit">Archiver</button>
							</div>
						</form>
						<div class="text-center" id="f-name" style="margin-top: 10px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
