<!-- Modal Ajouter un Candidat -->
<div class="modal fade" id="modAdd_note_e" tabindex="-1" role="dialog" aria-labelledby="Modify Mdp">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Ajouter la note pour ce candidat</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12">
						<form id="fm_note_ent" role="form" method="POST" action="{{ route('add_note_entretien', $cdt) }}">
							{{ csrf_field() }}

							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

							<div class="form-group{{ $errors->has('note_ent') ? ' has-error' : '' }} required">
								<label for="note_ent" class="control-label">Note</label>

								<input id="note_ent" type="text" class="form-control" name="note_ent" placeholder="Note ex: 3-" value="{{ $cdt->cdt_note_entretien }}" autofocus required>

								@if ($errors->has('note_ent'))
									<span class="help-block">
										<strong>{{ $errors->first('note_ent') }}</strong>
									</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('nom_jury_ent1') ? ' has-error' : '' }} required">
								<label for="nom_jury_ent1" class="control-label">Nom du membre de jury 1</label>

								<input id="nom_jury_ent1" type="text" class="form-control" name="nom_jury_ent1" placeholder="Nom" value="{{ $cdt->cdt_jury_entretien1 }}">

								@if ($errors->has('nom_jury_ent1'))
									<span class="help-block">
										<strong>{{ $errors->first('nom_jury_ent1') }}</strong>
									</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('nom_jury_ent2') ? ' has-error' : '' }} required">
								<label for="nom_jury_ent2" class="control-label">Nom du membre de jury 2</label>

								<input id="nom_jury_ent2" type="text" class="form-control" name="nom_jury_ent2" placeholder="Nom" value="{{ $cdt->cdt_jury_entretien2 }}">

								@if ($errors->has('nom_jury_ent2'))
									<span class="help-block">
										<strong>{{ $errors->first('nom_jury_ent2') }}</strong>
									</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('nom_jury_ent3') ? ' has-error' : '' }} required">
								<label for="nom_jury_ent3" class="control-label">Nom du membre de jury 3</label>

								<input id="nom_jury_ent3" type="text" class="form-control" name="nom_jury_ent3" placeholder="Nom" value="{{ $cdt->cdt_jury_entretien3 }}">

								@if ($errors->has('nom_jury_ent3'))
									<span class="help-block">
										<strong>{{ $errors->first('nom_jury_ent3') }}</strong>
									</span>
								@endif
							</div>

							<div class="form-group required">
								<label class="control-label">Champs obligatoires</label>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary">
										Enregistrer
									</button>
									<button class="btn btn-danger pull-right" data-dismiss="modal" aria-label="Close">
										Retour
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>