@extends('layouts.app')

@section('content')
<div class="container">
	@if(Auth::check())
		<!-- Errors -->
		<div class="row">
			<div class="col-md-12">
				@include('layouts.errors.errors')
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<ul class="nav nav-pills">
							<li><a href="#" data-toggle="modal" data-target="#modalImport"><i class="material-icons">&#xE2C4;</i> Importer</a></li>
							<li><a href="#" data-toggle="modal" data-target="#modAdd_candidat"><i class="material-icons">&#xE146;</i> Ajouter des candidats</a></li>
							<li><a href="{{ route('export') }}"><i class="material-icons">&#xE2C6;</i> Exporter</a></li>
							<li><a href="#" data-toggle="modal" data-target="#modArchive"><i class="material-icons">&#xE149;</i> Archiver</a></li>
							<li><a href="#" data-toggle="modal" data-target="#modDel_campaign"><i class="material-icons">&#xE872;</i> Supprimer</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active">
						<div class="panel panel-default">
							<div class="panel-heading">
								Candidats 
								<div class="pull-right">
									Total :
									<span class="badge">
										{{ $all_c2->count() + $prepa->count() + $etrangeres->count() }}
									</span>
								</div>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-12">
										<!-- Nav tabs -->
										<ul class="nav nav-tabs" role="tablist">
											<li class="active">
												<a href="#ciel2" role="tab" data-toggle="tab">
													E-candidat 
													<span class="badge">{{ $all_c2->count() }}</span>
												</a>
											</li>
											<li>
												<a href="#prepa" role="tab" data-toggle="tab">
													Prépa 
													<span class="badge">{{ $prepa->count() }}</span>
												</a>
											</li>
											<li>
												<a href="#foreign" role="tab" data-toggle="tab">
													Etrangers 
													<span class="badge">{{ $etrangeres->count() }}</span>
												</a>
											</li>
											<li>
												<a href="#tous" role="tab" data-toggle="tab">
													Tous 
													<span class="badge">
														{{ $all_c2->count() + $prepa->count() + $etrangeres->count() }}
													</span>
												</a>
											</li>
											<li>
												<a href="#archive" role="tab" data-toggle="tab">
													Archive
												</a>
											</li>
										</ul>

										<!-- Tab panes -->
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active" id="ciel2">
												<div class="row">
													<div class="col-md-12 col-xs-12 text-center">
														{{ $ciel2->links() }}
													</div>
												</div>
												<div class="table-responsive">
													<table class="table table-striped task-table">
														<thead>
															<tr>
																<!-- table header cells-->
																@include('layouts.tables.tb_header')
																<th width="180" class="text-center">
																	<div>Status</div>
																	<div>
																		<span class="badge red">{{ $all_c2->where('cdt_status', 'refuse')->count() }}</span>
																		<span class="badge green">
																			@if($all_c2->where('cdt_status', 'INT')->count() > 0)
																				<span class="text-success">
																					{{ $all_c2->where('cdt_status', 'INT')->count() }}
																				</span>/
																				<span class="text-danger">
																					{{ $all_c2->where('cdt_status', 'DEM')->count() }}
																				</span>/
																				{{ (
																						$all_c2->where('cdt_status', 'accepte')->count() + 
																						$all_c2->where('cdt_status', 'INT')->count() +
																						$all_c2->where('cdt_status', 'DEM')->count()
																					)
																				}}
																			@else
																				{{ $all_c2->where('cdt_status', 'accepte')->count() + $all_c2->where('cdt_status', 'DEM')->count() }}
																			@endif
																		</span>
																		<span class="badge yellow">{{ $all_c2->where('cdt_status', 'LC')->count() }}</span>
																		<span class="badge blue">{{ $all_c2->where('cdt_status', 'ET')->count() }}</span>
																	</div>
																</th>
															</tr>
														</thead>
														<tbody> 
															@php $i = 0; @endphp
															@foreach($ciel2 as $cdt)
																@php $i++; @endphp
																@if($cdt->cdt_status == 'refuse')
																	<!-- table row declined-->
																	@include('layouts.tables.tr_declined')
																@elseif($cdt->cdt_status == 'accepte')
																	<!-- table row accepted-->
																	@include('layouts.tables.tr_accepted')
																@elseif($cdt->cdt_status == 'LC')
																	<!-- table row comp-->
																	@include('layouts.tables.tr_comp')
																@elseif($cdt->cdt_status == 'ET')
																	<!-- table row entr-->
																	@include('layouts.tables.tr_entr')
																@elseif($cdt->cdt_status == 'INT')
																	<!-- table row integr-->
																	@include('layouts.tables.tr_integr')
																@elseif($cdt->cdt_status == 'DEM')
																	<!-- table row demis-->
																	@include('layouts.tables.tr_demis')
																@else
																	<!-- table row with status btns-->
																	@include('layouts.tables.tr_buttons')
																@endif
															@endforeach
														</tbody>
													</table>
												</div>
												<div class="row">
													<div class="col-md-12 col-xs-12 text-center">
														{{ $ciel2->links() }}
													</div>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="prepa">
												<div class="table-responsive">
													<table class="table table-striped task-table">
														<thead>
															<tr>
																<!-- table header cells-->
																@include('layouts.tables.tb_header')
																<th width="180" class="text-center">
																	<div>Status</div>
																	<div>
																		<span class="badge red">{{ $prepa->where('cdt_status', 'refuse')->count() }}</span>
																		<span class="badge green">
																			@if($prepa->where('cdt_status', 'INT')->count() > 0)
																				<span class="text-success">
																					{{ $prepa->where('cdt_status', 'INT')->count() }}
																				</span>/
																				<span class="text-danger">
																					{{ $prepa->where('cdt_status', 'DEM')->count() }}
																				</span>/
																				{{
																					$prepa->where('cdt_status', 'accepte')->count() + 
																					$prepa->where('cdt_status', 'INT')->count() +
																					$prepa->where('cdt_status', 'DEM')->count()
																				}}
																			@else
																				{{ $prepa->where('cdt_status', 'accepte')->count() + $prepa->where('cdt_status', 'DEM')->count() }}
																			@endif
																		</span>
																		<span class="badge yellow">{{ $prepa->where('cdt_status', 'LC')->count() }}</span>
																		<span class="badge blue">{{ $prepa->where('cdt_status', 'ET')->count() }}</span>
																	</div>
																</th>
															</tr>
														</thead>
														<tbody>
															@php $i = 0; @endphp
															@foreach($prepa as $cdt)
																@php $i++; @endphp
																@if($cdt->cdt_status == 'refuse')
																	<!-- table row declined-->
																	@include('layouts.tables.tr_declined')
																@elseif($cdt->cdt_status == 'accepte')
																	<!-- table row accepted-->
																	@include('layouts.tables.tr_accepted')
																@elseif($cdt->cdt_status == 'LC')
																	<!-- table row comp-->
																	@include('layouts.tables.tr_comp')
																@elseif($cdt->cdt_status == 'ET')
																	<!-- table row entr-->
																	@include('layouts.tables.tr_entr')
																@elseif($cdt->cdt_status == 'INT')
																	<!-- table row integr-->
																	@include('layouts.tables.tr_integr')
																@elseif($cdt->cdt_status == 'DEM')
																	<!-- table row demis-->
																	@include('layouts.tables.tr_demis')
																@else
																	<!-- table row with status btns-->
																	@include('layouts.tables.tr_buttons')
																@endif
															@endforeach
														</tbody>
													</table>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="foreign">
												<div class="table-responsive">
													<table class="table table-striped task-table">
														<thead>
															<tr>
																<!-- table header cells-->
																@include('layouts.tables.tb_header')
																<th width="180" class="text-center">
																	<div>Status</div>
																	<div>
																		<span class="badge red">{{ $etrangeres->where('cdt_status', 'refuse')->count() }}</span>
																		<span class="badge green">
																			@if($etrangeres->where('cdt_status', 'INT')->count() > 0)
																				<span class="text-success">
																					{{ $etrangeres->where('cdt_status', 'INT')->count() }}
																				</span>/
																				<span class="text-danger">
																					{{ $etrangeres->where('cdt_status', 'DEM')->count() }}
																				</span>/
																				{{
																					$etrangeres->where('cdt_status', 'accepte')->count() + 
																					$etrangeres->where('cdt_status', 'INT')->count() +
																					$etrangeres->where('cdt_status', 'DEM')->count()
																				}}
																			@else
																				{{ $etrangeres->where('cdt_status', 'accepte')->count() + $etrangeres->where('cdt_status', 'DEM')->count() }}
																			@endif
																		</span>
																		<span class="badge yellow">{{ $etrangeres->where('cdt_status', 'LC')->count() }}</span>
																		<span class="badge blue">{{ $etrangeres->where('cdt_status', 'ET')->count() }}</span>
																	</div>
																</th>
															</tr>
														</thead>
														<tbody>
															@php $i = 0; @endphp
															@foreach($etrangeres as $cdt)
																@php $i++; @endphp
																@if($cdt->cdt_status == 'refuse')
																	<!-- table row declined-->
																	@include('layouts.tables.tr_declined')
																@elseif($cdt->cdt_status == 'accepte')
																	<!-- table row accepted-->
																	@include('layouts.tables.tr_accepted')
																@elseif($cdt->cdt_status == 'LC')
																	<!-- table row comp-->
																	@include('layouts.tables.tr_comp')
																@elseif($cdt->cdt_status == 'ET')
																	<!-- table row entr-->
																	@include('layouts.tables.tr_entr')
																@elseif($cdt->cdt_status == 'INT')
																	<!-- table row integr-->
																	@include('layouts.tables.tr_integr')
																@elseif($cdt->cdt_status == 'DEM')
																	<!-- table row demis-->
																	@include('layouts.tables.tr_demis')
																@else
																	<!-- table row with status btns-->
																	@include('layouts.tables.tr_buttons')
																@endif
															@endforeach
														</tbody>
													</table>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="tous">
												<div class="table-responsive">
													<table class="table table-striped task-table">
														<thead>
															<tr>
																<!-- table header cells-->
																@include('layouts.tables.tb_header')

																<th width="180" class="text-center">
																	<div>Status</div>
																	<div>
																		<span class="badge red">{{ $all->where('cdt_status', 'refuse')->count() }}</span>
																		<span class="badge green">
																			@if($all->where('cdt_status', 'INT')->count() > 0)
																				<span class="text-success">
																					{{ $all->where('cdt_status', 'INT')->count() }}
																				</span>/
																				<span class="text-danger">
																					{{ $all->where('cdt_status', 'DEM')->count() }}
																				</span>/
																				{{
																					$all->where('cdt_status', 'accepte')->count() + 
																					$all->where('cdt_status', 'INT')->count() +
																					$all->where('cdt_status', 'DEM')->count()
																				}}
																			@else
																				{{ $all->where('cdt_status', 'accepte')->count() + $all->where('cdt_status', 'DEM')->count() }}
																			@endif
																		</span>
																		<span class="badge yellow">{{ $all->where('cdt_status', 'LC')->count() }}</span>
																		<span class="badge blue">{{ $all->where('cdt_status', 'ET')->count() }}</span>
																	</div>
																</th>
															</tr>
														</thead>
														<tbody>
															@php $i = 0; @endphp
															@foreach($all as $cdt)
																@php $i++; @endphp
																@if($cdt->cdt_status == 'refuse')
																	<!-- table row declined-->
																	@include('layouts.tables.tr_declined')
																@elseif($cdt->cdt_status == 'accepte')
																	<!-- table row accepted-->
																	@include('layouts.tables.tr_accepted')
																@elseif($cdt->cdt_status == 'LC')
																	<!-- table row comp-->
																	@include('layouts.tables.tr_comp')
																@elseif($cdt->cdt_status == 'ET')
																	<!-- table row entr-->
																	@include('layouts.tables.tr_entr')
																@elseif($cdt->cdt_status == 'INT')
																	<!-- table row integr-->
																	@include('layouts.tables.tr_integr')
																@elseif($cdt->cdt_status == 'DEM')
																	<!-- table row demis-->
																	@include('layouts.tables.tr_demis')
																@else
																	<!-- table row with status btns-->
																	@include('layouts.tables.tr_buttons')
																@endif
															@endforeach
														</tbody>
													</table>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="archive">
												<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
													@foreach($archive as $year)
														@php $i++; @endphp
														<div class="panel panel-default">
															<div class="panel-heading" role="tab" id="year_{{ $year->pluck('cdt_year')->first() }}">
																<h4 class="panel-title">
																	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#c_year_{{ $year->pluck('cdt_year')->first() }}" aria-expanded="true" aria-controls="c_year_{{ $year->pluck('cdt_year')->first() }}">
																		Année - {{ $year->pluck('cdt_year')->first() }}
																	</a> 
																	<span class="badge red">{{ $year->where('cdt_status', 'refuse')->count() }}</span>
																	<span class="badge green">
																		@if($year->where('cdt_status', 'INT')->count() > 0)
																			<span class="text-success">
																				{{ $year->where('cdt_status', 'INT')->count() }}
																			</span>/
																			<span class="text-danger">
																				{{ $year->where('cdt_status', 'DEM')->count() }}
																			</span>/
																			{{
																				$year->where('cdt_status', 'accepte')->count() + 
																				$year->where('cdt_status', 'INT')->count() +
																				$year->where('cdt_status', 'DEM')->count()
																			}}
																		@else
																			{{ $year->where('cdt_status', 'accepte')->count() + $year->where('cdt_status', 'DEM')->count() }}
																		@endif
																	</span>
																	<span class="badge yellow">{{ $year->where('cdt_status', 'LC')->count() }}</span>
																	<span class="badge blue">{{ $year->where('cdt_status', 'ET')->count() }}</span>

																	<span class="pull-right">
																		Nombre total des candidats : 
																		<span class="badge">{{ $year->count() }}</span>
																		<a href="#" class="text-danger" data-placement="top" title="Supprimer tous les données pour cette année" data-tooltip="true" data-toggle="modal" data-year="{{ $year->pluck('cdt_year')->first() }}" data-target="#modDel_from_archive"><i class="material-icons md-18">close</i></a>
																	</span>
																</h4>
															</div>
															<div id="c_year_{{ $year->pluck('cdt_year')->first() }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="year_{{ $year->pluck('cdt_year')->first() }}">
																<div class="panel-body">
																	<div class="table-responsive">
																		<table class="table table-striped task-table">
																			<thead>
																				<tr>
																					<!-- table header cells-->
																					@include('layouts.tables.tb_header')
																					<th width="160">
																						<div>Status</div>
																						<div>
																							<span class="badge red">{{ $year->where('cdt_status', 'refuse')->count() }}</span>
																							<span class="badge green">
																								@if($year->where('cdt_status', 'INT')->count() > 0)
																									<span class="text-success">
																										{{ $year->where('cdt_status', 'INT')->count() }}
																									</span>/
																									<span class="text-danger">
																										{{ $year->where('cdt_status', 'DEM')->count() }}
																									</span>/
																									{{
																										$year->where('cdt_status', 'accepte')->count() + 
																										$year->where('cdt_status', 'INT')->count() +
																										$year->where('cdt_status', 'DEM')->count()
																									}}
																								@else
																									{{ $year->where('cdt_status', 'accepte')->count() + $year->where('cdt_status', 'DEM')->count() }}
																								@endif
																							</span>
																							<span class="badge yellow">{{ $year->where('cdt_status', 'LC')->count() }}</span>
																							<span class="badge blue">{{ $year->where('cdt_status', 'ET')->count() }}</span>
																						</div>
																					</th>
																				</tr>
																			</thead>
																			<tbody> 
																				@php $i = 0; @endphp
																				@foreach($year as $cdt)
																					@php $i++; @endphp
																					@if($cdt->cdt_status == 'refuse')
																						<!-- table row declined-->
																						@include('layouts.tables.tr_declined')
																					@elseif($cdt->cdt_status == 'accepte')
																						<!-- table row accepted-->
																						@include('layouts.tables.tr_accepted')
																					@elseif($cdt->cdt_status == 'LC')
																						<!-- table row comp-->
																						@include('layouts.tables.tr_comp')
																					@elseif($cdt->cdt_status == 'ET')
																						<!-- table row entr-->
																						@include('layouts.tables.tr_entr')
																					@elseif($cdt->cdt_status == 'INT')
																						<!-- table row integr-->
																						@include('layouts.tables.tr_integr')
																					@elseif($cdt->cdt_status == 'DEM')
																						<!-- table row demis-->
																						@include('layouts.tables.tr_demis')
																					@else
																						<!-- table row with status btns-->
																						@include('layouts.tables.tr_buttons')
																					@endif
																				@endforeach
																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													@endforeach
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif
</div>
@endsection

<!-- Modal Changing Password -->
@include('modals.change_pass')

<!-- Modal Import -->
@include('modals.import_ciel')

<!-- Modal Ajouter un Candidat -->
@include('modals.add_candidat')

<!-- Modal Changer Status -->
@include('modals.change_status')

<!-- Modal Archive data -->
@include('modals.archive')

<!-- Modal Archive data -->
@include('modals.delete_campaign')

<!-- Modal Archive data -->
@include('modals.delete_from_archive')