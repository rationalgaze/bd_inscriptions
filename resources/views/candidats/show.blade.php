@extends('layouts.app')

@section('content')
<div class="container">

	<!-- Errors -->
	<div class="row">
		<div class="col-md-12">
			@include('layouts.errors.errors')
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h2>
				<strong>{{ $cdt->cdt_nom." ".$cdt->cdt_prenom }}</strong> 
				@if($cdt->cdt_code_ine_bea) 
					<small>INE : {{ $cdt->cdt_code_ine_bea }}</small>
				@endif
				<span>
					<a href="#" data-toggle="modal" data-target="#modMod_candidat" data-note_entretien="{{ $cdt->cdt_id }}">
						<i class="material-icons md-18">edit</i>
					</a>
				</span>
			</h2>
			<p>
				<i class="material-icons md-18">mail_outline</i>
				<a href="mailto:{{ $cdt->cdt_email }}">
					{{ $cdt->cdt_email }}
				</a>
				@if($cdt->cdt_tel_mob)
					<i class="material-icons md-18">phone_iphone</i>
					{{ chunk_split($cdt->cdt_tel_mob, 2, " ") }}
				@elseif($cdt->cdt_tel_fixe)
					<i class="material-icons md-18">phone</i>
					{{ chunk_split($cdt->cdt_tel_fixe, 2, " ") }}
				@endif
			</p>

			<p><strong>Date de naissance : </strong>{{ Carbon\Carbon::parse($cdt->cdt_date_de_naissance)->format('d/m/Y') }}</p>

			<p><strong>Adresse : </strong>{{ $cdt->cdt_adresse.", ".$cdt->cdt_cp." ". $cdt->cdt_ville." ".$cdt->cdt_pays }}</p>

			@if($cdt->cdt_nationalite)
				<p class="text-capitalize"><strong>Nationalité : </strong>{{ strtolower($cdt->cdt_nationalite) }}</p>
			@endif

			@if($cdt->cdt_formation && $cdt->cdt_formation != '-')
				<p><strong>Formation :</strong> {{ $cdt->cdt_formation }}</p>
			@endif

			@if($cdt->cdt_mention_bac == 'TB')
				<p><strong>Mention BAC : </strong>Très bien</p>
			@elseif($cdt->cdt_mention_bac == 'AB')
				<p><strong>Mention BAC : </strong>Assez bien</p>
			@elseif($cdt->cdt_mention_bac == 'B')
				<p><strong>Mention BAC : </strong>Bien</p>
			@elseif($cdt->cdt_mention_bac == 'P')
				<p><strong>Mention BAC : </strong>Passable ou sans mention</p>
			@endif

			<p>
				<strong>Postbac :</strong> <br>
				<span style="margin-left: 20px;"><em>{{ $cdt->cdt_postbac_intitule }} </em></span><br>
				<span style="margin-left: 20px;">{{ $cdt->cdt_postbac_etablissement }} {{ $cdt->cdt_postbac_lieu }}</span>
			</p>

			@if($cdt->cdt_diplome_type || $cdt->cdt_diplome_intitule || $cdt->cdt_diplome_etablissement || $cdt->cdt_diplome_type_etablissement)
				<p><strong>Diplome :</strong> <br>
					<span style="margin-left: 20px;"><em>{{ $cdt->cdt_diplome_type }}</em></span> <br>
					<span style="margin-left: 20px;"><em>{{ $cdt->cdt_diplome_intitule }}</em></span> <br>
					<span style="margin-left: 20px;">{{ $cdt->cdt_diplome_etablissement }}</span> <br>
				</p>
			@endif

			@if($cdt->cdt_parcours)
				<p><strong>Parcours : </strong><span>{{ $cdt->cdt_parcours }}</span></p>
			@endif

			@if($cdt->cdt_position_voeu)
				<p><strong>Position vœu : </strong><span>{{ $cdt->cdt_position_voeu }}</span></p>
			@endif

			@if($cdt->cdt_rang)
				<p><strong>Rang : </strong><span>{{ $cdt->cdt_rang }}</span></p>
			@endif

			@if($cdt->cdt_note_ang_bac)
				<p><strong>Note d'anglais au BAC : </strong><span>{{ $cdt->cdt_note_ang_bac }}</span></p>
			@endif

			@if($cdt->cdt_note_ang_univ)
				<p><strong>Note d'anglais à l'université :</strong><span>{{ $cdt->cdt_note_ang_univ }}</span></p>
			@endif

			@if($cdt->cdt_toeic)
				<p><strong>Certificat d'anglais : </strong><span>{{ $cdt->cdt_toeic }}</span></p>
			@endif

			@if($cdt->cdt_toeic && $cdt->cdt_score_eng)
				<p><strong>Score : </strong><span>{{ $cdt->cdt_score_eng }}</span></p>
			@endif

			<p>
				<strong>Note de dossier : </strong>
				@if($cdt->cdt_note_dossier)
					<a href="#" data-toggle="modal" data-target="#modAdd_note_d" data-note_dossier="{{ $cdt->cdt_id }}">
						<span class="text-danger">
							<strong>{{ $cdt->cdt_note_dossier }}</strong>
						</span>
					</a>
					<strong>jury :</strong> {{ $cdt->cdt_jury_dossier1."/".$cdt->cdt_jury_dossier2 }}
				@else
					<a href="#" data-toggle="modal" data-target="#modAdd_note_d" data-note_dossier="{{ $cdt->cdt_id }}">
						<i class="material-icons md-18">edit</i>
					</a>
				@endif
			</p>

			<p>
				<strong>Note d'entretien : </strong>
				@if($cdt->cdt_note_entretien)
					<a href="#" data-toggle="modal" data-target="#modAdd_note_e" data-note_entretien="{{ $cdt->cdt_id }}">
						<span class="text-danger">
							<strong>{{ $cdt->cdt_note_entretien }}</strong>
						</span>
					</a>
					<strong>jury :</strong> {{ $cdt->cdt_jury_entretien1."/".$cdt->cdt_jury_entretien2."/".$cdt->cdt_jury_entretien3 }}
				@else
					<a href="#" data-toggle="modal" data-target="#modAdd_note_e" data-note_entretien="{{ $cdt->cdt_id }}">
						<i class="material-icons md-18">edit</i>
					</a>
				@endif
			</p>

			<p>
				<strong>Statut : </strong> 
				@if($cdt->cdt_status == 'accepte')
					<a href="#" data-toggle="modal" data-target="#modChange_status" data-chg="{{ $cdt->cdt_id }}" class="text-success">
						<span>Accepté</span>
						<i class="material-icons text-success md-24">check</i>
					</a>
				@elseif($cdt->cdt_status == 'refuse')
					<a href="#" data-toggle="modal" data-target="#modChange_status" data-chg="{{ $cdt->cdt_id }}" class="text-danger">
						<span>Refusé</span>
						<i class="material-icons text-danger md-24">close</i>
					</a>
				@elseif($cdt->cdt_status == 'LC')
					<a href="#" data-toggle="modal" data-target="#modChange_status" data-chg="{{ $cdt->cdt_id }}" class="text-warning">
						<span class="text-warning">Liste Complementaire</span>
					</a>
				@elseif($cdt->cdt_status == 'ET')
					<a href="#" data-toggle="modal" data-target="#modChange_status" data-chg="{{ $cdt->cdt_id }}" class="text-info">
						<span class="text-info">Convoqué à l'entretien</span>
					</a>
				@elseif($cdt->cdt_status == 'INT')
					<a href="#" data-toggle="modal" data-target="#modChange_status" data-chg="{{ $cdt->cdt_id }}">
						<span class="text-success">Integré
							<i class="material-icons md-24">sentiment_very_satisfied</i>
						</span>
					</a>
				@elseif($cdt->cdt_status == 'DEM')
					<a href="#" data-toggle="modal" data-target="#modChange_status" data-chg="{{ $cdt->cdt_id }}">
						<span class="text-danger">Démissionné
							<i class="material-icons">sentiment_dissatisfied</i>
						</span>
					</a>
				@else
					<div class="btns-stat">
						<form class="status-fm" action="{{ route('decline', $cdt->cdt_id) }}" method="POST">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-xs">
								<i class="material-icons md-18">close</i>
							</button>
						</form>
						<form class="status-fm" action="{{ route('accept', $cdt->cdt_id) }}" method="POST">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-success btn-xs">
								<i class="material-icons md-18">check</i>
							</button>
						</form>
						<form class="status-fm" action="{{ route('lc', $cdt->cdt_id) }}" method="POST">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-warning btn-xs">
								<strong>LC</strong>
							</button>
						</form>
						<form class="status-fm" action="{{ route('et', $cdt->cdt_id) }}" method="POST">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-info btn-xs">
								<strong>ET</strong>
							</button>
						</form>
						<form class="status-fm" action="{{ route('integrer', $cdt->cdt_id) }}" method="POST">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-success btn-xs">
								<i class="material-icons md-18">sentiment_very_satisfied</i>
							</button>
						</form>
						<form class="status-fm" action="{{ route('dem', $cdt->cdt_id) }}" method="POST">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-xs">
								<i class="material-icons md-18">sentiment_dissatisfied</i>
							</button>
						</form>
					</div>
				@endif
			</p>

		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a href="{{ route('candidats.dashboard') }}" class="btn btn-default btn-md btn-float">
				<i class="material-icons">keyboard_backspace</i>
			</a>
		</div>
	</div>
</div>
@endsection

<!-- Modal Changer Status -->
@include('modals.change_status')

<!-- Modal Changer Status -->
@include('modals.add_note_dossier')

<!-- Modal Changer Status -->
@include('modals.add_note_entretien')

<!-- Modal Changer Status -->
@include('modals.mod_candidat')