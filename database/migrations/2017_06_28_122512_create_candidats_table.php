<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('candidats', function (Blueprint $table) {
			$table->increments('cdt_id');
			$table->string('cdt_type', 1)->nullable();
			$table->string('cdt_ciel_id', 10)->unique()->nullable();
			$table->string('cdt_civilite', 3)->nullable();
			$table->string('cdt_nom', 50)->nullable();
			$table->string('cdt_nom_de_naissance', 50)->nullable();
			$table->string('cdt_prenom', 50)->nullable();
			$table->date('cdt_date_de_naissance', 50)->nullable();

			$table->string('cdt_nationalite', 100)->nullable();
			$table->string('cdt_adresse', 100)->nullable();
			$table->string('cdt_cp', 10)->nullable();
			$table->string('cdt_ville', 50)->nullable();
			$table->string('cdt_pays', 50)->nullable();
			$table->string('cdt_tel_mob', 16)->nullable();
			$table->string('cdt_tel_fixe', 16)->nullable();
			$table->string('cdt_email', 190)->nullable();
			$table->string('cdt_code_ine_bea', 50)->nullable();

			$table->string('cdt_formation', 200)->nullable();
			$table->string('cdt_postbac_etablissement', 50)->nullable();
			$table->string('cdt_postbac_intitule', 150)->nullable();
			$table->string('cdt_postbac_lieu', 50)->nullable();
			$table->string('cdt_diplome_etablissement', 150)->nullable();
			$table->string('cdt_diplome_type_etablissement', 80)->nullable();
			$table->string('cdt_diplome_intitule', 150)->nullable();
			$table->string('cdt_diplome_type', 50)->nullable();

			// Candidats etrangers et papieres
			$table->string('cdt_parcours', 5)->nullable();
			$table->string('cdt_position_voeu', 1)->nullable();
			$table->string('cdt_rang', 50)->nullable();
			$table->string('cdt_mention_bac', 50)->nullable();
			$table->string('cdt_note_ang_bac', 50)->nullable();
			$table->string('cdt_note_ang_univ', 50)->nullable();
			$table->string('cdt_toeic', 50)->nullable();
			$table->string('cdt_score_eng', 50)->nullable();

			$table->char('cdt_note_dossier', 2)->nullable();
			$table->char('cdt_note_entretien', 2)->nullable();
			$table->char('cdt_jury_dossier1', 2)->nullable();
			$table->char('cdt_jury_dossier2', 2)->nullable();
			$table->char('cdt_jury_entretien1', 2)->nullable();
			$table->char('cdt_jury_entretien2', 2)->nullable();
			$table->char('cdt_jury_entretien3', 2)->nullable();
			$table->string('cdt_status', 10)->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('candidats');
	}
}
